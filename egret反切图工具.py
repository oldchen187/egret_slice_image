#!/usr/bin/python
# -*- coding: UTF-8 -*-
import os
import re
import sys
import json
from PIL import Image


#获取根路径
global_root = os.getcwd()
#资源根路径
source_root = global_root + "\Source"
#输出根路径
out_root = global_root + "\Total"
#剪切对象数组
splice_arrs = []

class SpliceImage:
    img_str = ""  #要切割图像所在的根路径
    img_arr = []  #根路径下的图片名称数组
    img_jso = []  #根路径下的JSON名称数组
    json_obj = [] #json对象数组

    def __init__(self):
        self.img_str = ""
        self.img_arr = []
        self.img_jso = []
        self.json_obj = []

    def ReadSource(self,fs:[]):
        for str in fs:
            if re.search("png|jpg",str) != None:
                self.img_arr.append(str)
            if re.search("json",str) != None:
                self.img_jso.append(str)

#文件扫描
def FileCheck():
    for root, dirs, files in os.walk(source_root):
        if len(files) > 0:
            new_spi = SpliceImage()
            new_spi.img_str = root
            splice_arrs.append(new_spi)
            continue

#资源名读取
def SourceCheck():
    for obj in splice_arrs:
        temp_str = obj.img_str
        for rt, ds, fs in os.walk(temp_str):
            obj.ReadSource(fs)
            break

#Json资源文件解析
def JsonFileParsing():
    for obj in splice_arrs:
        json_str = obj.img_str
        for jso in obj.img_jso:
            json_obj = JsonFileControl(json_str,jso)
            if json_obj != None:
                obj.json_obj.append(json_obj)
            else:
                obj.json_obj.append({})
                continue

#JSON文件操作
#str JSON文件路径
#nam JSON文件名称
def JsonFileControl(str,nam):
    json_obj = {}
    json_str = os.path.join(str,nam)
    json_file = open(json_str,"r",encoding="utf-8")
    json_text = json_file.read()
    json_obj = json.loads(json_text)
    json_file.close()
    return json_obj

#创建剪切盒元组
#json_res 资源对象信息
def CreateSpliceBox(json_res):
    x_1 = json_res["x"]
    x_2 = json_res["w"]
    y_1 = json_res["y"]
    y_2 = json_res["h"]
    box = (x_1,y_1,x_1 + x_2,y_1 + y_2)
    return box

#对应匹配资源
#json_name json资源名
#pict_arrs 图片资源名数组
def MatchSource(json_name,pict_arrs):
    json_n = re.sub(".json","",json_name)
    for img_str in pict_arrs:
        if json_n == re.sub(".png|.jpg","",img_str):
            return img_str
    return ""

#返回图片文件类型
def ReturnTypeImg(img_name):
    types = ""
    if re.search("png",img_name) != None:
        types = ".png"
    if re.search("jpg", img_name) != None:
        type = ".jpg"
    return types

#图片剪切
#box 剪切盒
#nam_o 原图片名
#nam_n 新图片名
#rot 原图片所在路径
def SplicePicture(box,nam_o,nam_n,rot):
    dir_name = re.sub(".jpg|.png","",nam_o)
    new_dir = out_root + "\\" + dir_name;
    pic_name = "\\" + nam_o;
    pil_im = Image.open(rot + pic_name);
    #print(pil_im)
    pil_splice = pil_im.crop(box)
    #判断新文件夹是否存在,不存在则创建
    if not os.path.exists(new_dir):
        os.makedirs(new_dir)
    #保存图片到新文件夹
    pil_splice.save(new_dir + "\\" + nam_n)

#图片裁切总操作
def TotalSpliceImage():
    count = -1
    for obj in splice_arrs:
        for jso in obj.json_obj:
            count += 1
            is_move_ui = None
            img_json = obj.img_jso[count]
            img_o = MatchSource(img_json,obj.img_arr)
            types = ReturnTypeImg(img_o)
            if img_o == "":
                print(img_json+"对应的图片资源文件不存在！")
                continue
            if "frames" in jso.keys():
                is_move_ui = jso["frames"]
            if  "res" in jso.keys():
                is_move_ui = jso["res"]
            print("正在裁切图片" + img_o)
            for parm in is_move_ui:
                box_n = CreateSpliceBox(is_move_ui[parm])
                SplicePicture(box_n,img_o,parm + types,obj.img_str)



FileCheck()
SourceCheck()
JsonFileParsing()
TotalSpliceImage()
print("裁切完成!")
os.system("pause")

